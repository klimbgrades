# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: klimbgrades_qt\n"
"PO-Revision-Date: 2023-02-15 16:09+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Qt-Contexts: true\n"
"X-POFile-SpellExtra: Wikipedia\n"

#: Global.qml:45
msgctxt "Global|"
msgid "Reset"
msgstr "Reiniciar"

#: Global.qml:50
msgctxt "Global|"
msgid "Set Record"
msgstr "Definir como Recorde"

#: Global.qml:51
msgctxt "Global|"
msgid "Set Grade As Personal Record"
msgstr "Definir a Cotação como Recorde Pessoal"

#: Global.qml:58
msgctxt "Global|"
msgid "Clear"
msgstr "Limpar"

#: Global.qml:59
msgctxt "Global|"
msgid "Clear Personal Record"
msgstr "Limpar o Recorde Pessoal"

#: Global.qml:83
msgctxt "Global|"
msgid "Source: Wikipedia"
msgstr "Origem: Wikipedia"

#: GradeWidgetBase.qml:96
msgctxt "GradeWidgetBase|"
msgid "Record: "
msgstr "Recorde: "

#: Main.qml:54
msgctxt "Main|"
msgid "Climbing Grades"
msgstr "Subida das Cotações"

#: Main.qml:82 Main.qml:109 Main.qml:168
msgctxt "Main|"
msgid "Lead"
msgstr "Líder"

#: Main.qml:87 Main.qml:117 Main.qml:199
msgctxt "Main|"
msgid "Boulder"
msgstr "Auxiliar"

#: Main.qml:162
msgctxt "Main|"
msgid "Filter…"
msgstr "Filtro…"

#: Main.qml:232
msgctxt "Main|"
msgid "Link Lead and Boulder"
msgstr "Líder de Ligação e Auxiliar"
